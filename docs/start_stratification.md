# Request Stratification

**Stratification is a paid service of SoilStack.** Check out our [Pricing Guide](https://our-sci.gitlab.io/software/soilstack-tutorials/soilstack_pricing/) to find the plan that is right for you.

## Stratify Fields
Stratification is currently completed using a semi-manual process:

1. Contact info@soilstack.io to get started. 
- A SoilStack staff member will work with you to create stratification settings that match your project needs.
- A summary of stratification settings is available [here](https://our-sci.gitlab.io/software/soilstack-tutorials/stratification_settings/)
2. When fields are ready to be stratified, send a list of fields to be stratified to info@soilstack.io. Stratification will be complete within 2-3 business days and you will receive a notification upon completion.
- In short our stratification process involves:
     - The SoilStack webservice automatically fetches and clips ancillary data layers to the field boundary.
     - A k-means clustering algorithm creates strata based on the variability of the ancillary data layers.
     - Using cLHS, the stratification service uses in-stratum variability of the same ancillary data layers to optimize sampling locations.
- A more detailed description of the process is available [here](https://our-sci.gitlab.io/software/soilstack-tutorials/stratification_process/)


### Development of Automated Stratification Process
We are currently developing a push-button stratification solution that project administrators can access directly in SoilStack. We hope to release features supporting this service thoughout the 2024 fall season.


