# Add Field

## Using the API
Post fields directly to your SoilStack group using our API.

#### Create API Key
- Go to the profile icon in the upper right
- Select **Profile** from the dropdown menu.
- Select **Create Key**
- Enter a label to remember your api key by
- Select **Generate Key**. Your generated API key will be shown to you only once. 
- Save it securely and then select **I have saved my API key**.

#### Post fields via API: 
- Include it in an HTTP `Authorization` header when making requests to the SoilStack API (ex. `Authorization: apikey your-api-key-here`). 
- Full API documentation can be found [here](https://api.soilstack.io/api-docs/).

![API Key](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/API_key.png)

## Draw Fields in SoilStack

<iframe width="674" height="379" src="https://www.youtube.com/embed/f-WCxeyyYd4" title="Drawing a Field With SoilStack" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>


The field drawing tool can be reached at [https://app.soilstack.io/field/new](https://app.soilstack.io/field/new). It is best to use this tool on a computer

![Select from menu](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/select_from_menu.png)

When you are drawing the field, you can navigate to the general area by putting in the address in the upper right corner search bar. Then start with `+ Boundary` and draw the outline of the field. If you need to include another polygon, you can click the `+ Boundary` again after you have accepted the original boundary to draw a second polygon. If you need to remove an internal portion of the field, you can use the `+ Cutout` button and draw out the area you wish to exclude from the field, similar to how you drew the first boundary. 

![Draw field boundary](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/draw_field_boundary.png)

Add in details about your field. The only required fields on the form are "Name" and "Group". The "Group" option will be based on the groups you are a member of, and other admins of that group will have access to the new field. Including “Producer Name” will allow you to sort the fields by that name in the app when you are sampling. Including “Address” and “Contact” it will allow you to navigate to the farm address and make a call to the producer in the app.  

![Enter field details](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/enter_field_details.png)

![Enter field details 2](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/enter_field_details2.png)

Review the before submitting.

![Review field](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Add%20field/review.png)

## Edit Fields
### Edit field metadata
Select the pencil icon next to the field name from the Field Information Page. Make sure to select “SAVE” after editing the information.

![Edit Fields](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/edit_field.png)

### Edit Field Boundaries
**NOTE:** Field Boundaries can only be edited in the following situations:

- The field was drawn in SoilStack and *NOT* posted via the API **AND**
- The field has not yet been stratified.

To edit a field boundary, select the pencil icon next to the *Area* on the Field Information Page. Then you can use the field drawing tools to modify the boundaries, or delete a boundary and start over.

![Edit boundary](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/edit_boundary.png)
