# Accessing the app

<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=GA_MEASUREMENT_ID"></script> -->
<!-- <script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){window.dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-0HSX0DD016');
</script> -->

- If you are a part of a group, you need to log in to see your group's fields. Only group admins can add a user to a group.
- Follow these [instructions](https://our-sci.gitlab.io/software/soilstack-tutorials/manage_groups/) to create your own group


## Create an account
Create a SoilStack account by accepting an invitation or registering a new account.

### Accept a SoilStack invitation
Invitations are sent out by group administrators. 

- You will receive an email from SoilStack.io (noreply@soilstack.io) informing you that you have been added to [GROUP NAME] group. 
- Click **Create Account**
- On the Register page, create a password and select **SIGN UP**

![Email invitation](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/accept_invitation.jpg)

### Register an account
- Go to app.soilstack.io
- Select **Register now** from the login page.
- On the Register page, create a password and select **SIGN UP**


![Register account](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/register_account.jpg)

## Installing SoilStack
SoilStack is cross-platform, meaning that is works on **computers** and mobile devices, **android** and **iPhone**. It is a progressive web app, not a native app, which  means you do not go to the apple app store or google playstore to download the app. Instead, you go to app.soilstack.io and `Install` or `Add to Homescreen` depending on your device and browser type. Follow the links for more information on how to install the app in [Chrome](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-chrome), [Firefox](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-firefox), or [Safari](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#installing-soilstack-using-safari).

### Chrome

1. Go to app.soilstack.io, and select “Add SoilStack to Homescreen” from bottom 
2. If the option is not available, select the menu from the top right and select “Install App” 
3. Select “Install”
4. SoilStack will now be installed on your device and will work offline 

![Install with Chrome](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/chrome_all.png)

### Firefox 

1. Go to app.soilstack.io, and  select the menu from the  bottom right of the page 
2. Select “Install” 
3. Select “Add” 
4. SoilStack will now be installed on your device and will work offline 

![Install with Firefox](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/firefox.png)

### Safari 

1. Go to app.soilstack.io, and  select the options button on  the bottom of the page 
2. Select “Add to Home  Screen” 
3. Select “Add”
4. SoilStack will now be installed on your device and will work offline 

![Install with Safari](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Installing%20soilstack/safari_fox.png)


