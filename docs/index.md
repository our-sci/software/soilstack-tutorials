# Welcome to SoilStack 
**SoilStack is an open source app designed to support smart agricultural and environmental sampling, capturing patterns of spatial variability and guiding users through in-field collection in a transparent, replicable, and user-friendly way.**

## Quick Start Guide
### For Project Managers
- **Create an account and a group**.  Access to fields is based on [group and subgroup membership](https://our-sci.gitlab.io/software/soilstack-tutorials/manage_groups/).
- **Add fields**. Polygons can be [drawn](https://our-sci.gitlab.io/software/soilstack-tutorials/add_field/#draw-fields-in-soilstack) directly in SoilStack, but most projects use the SoilStack API to post fields to their SoilStack group.
     - Create an API key in your [profile](https://our-sci.gitlab.io/software/soilstack-tutorials/get_started_in_app/#api-keys).
     - [SoilStack API documentation](https://api.soilstack.io/api-docs/).
- **Stratify fields**. Stratification is currently completed using a semi-manual process as follows:
     - Contact info@soilstack.io to get started.
        - A SoilStack staff member will work with you to create the stratification settings that match your project needs.
        - A summary of stratification settings is available [here](https://our-sci.gitlab.io/software/soilstack-tutorials/stratification_settings/)
     - When fields are ready to be stratified, send a list of fields to be stratified to info@soilstack.io. Stratification will be complete in 2-3 business days and you will receive a notification once the fields are complete.
- [Add soil samplers to your group](https://our-sci.gitlab.io/software/soilstack-tutorials/manage_groups/add-members-to-groups-and-subgroups).

### For Soil Samplers
- [Accept the invitation](https://our-sci.gitlab.io/software/soilstack-tutorials/accessing_the_app/#accept-a-soilstack-invitation) to your project group.
- Once you log in, you will see fields that you have access to on the SoilStack [landing page](https://our-sci.gitlab.io/software/soilstack-tutorials/get_started_in_app/#fields-landing-page).
- Watch the SoilStack [tutorial video](https://our-sci.gitlab.io/software/soilstack-tutorials/before_sampling/#in-field-demo) to understand how to collect samples in the field.




