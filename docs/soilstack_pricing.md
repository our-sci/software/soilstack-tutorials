## SoilStack Price Guide

### How We Price
- **# of Fields.** Sampling plans are generated for each field entered in SoilStack.
- **Why?** Because producers manage at the field level.
- **Sampling plans never expire.** You will always be able to collect against a sampling plan, even if you’ve canceled your subscription.

### Choose the plan that’s right for you
Whether you’re a farmer or agronomist creating sampling plans for a few fields or you’re managing a carbon program with thousands of fields, SoilStack has a pricing plan that’s right for you.

#### Annual Subscriptions
**Annual Subscriptions** are best for large projects with hundreds to thousands of fields. Upgrade your subscription at any time, so you only pay for more fields when you need them.

|Fields stratified / year|Cost|
|------------------------|----|
|0-100 Fields| $750|
|101-500 Fields| $2500|
|501-5000 Fields| $5000| 
|5000+ Fields| Contact info@soilstack.io|

#### Field Bundles
**Field Bundles** offer a cost-effective option for small projects.

|Bundle|Cost|
|------------------------|----|
|1 Field| $10|
|20 Fields| $180|
|50 Fields| $400| 

#### Additional services
- **Consulting Services:** Our experts can help develop sampling plans that fit your project needs and budget.
- **Integration Support:** Our team of developers can assist in connecting to the SoilStack API to push field boundaries to SoilStack or to retrieve stratification and sampling collection data from SoilStack.

Contact info@soilstack.io to enquire about prices for these services



