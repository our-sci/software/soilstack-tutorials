# Managing Groups and Users

**What is a Group?**
A **Group** is a set of users (people or institutions) who are collaborating. Permission to access fields is based on the location that a field is assigned to. All users in a group currently have the same access to edit fields and complete sampling collections.

## Create a Group
1. Open the left-side menu and select **Groups**
2. Select **ADD GROUP**
3. Enter the group name and select **ADD GROUP**

![Create Group](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Manage%20Groups/create_group.jpg)

## Subgroups
A subgroup is basically a group within a group. It can help with organization and allow group admins to restrict access to fields by placing fields in different subgroups. For example, if a project involves two soil sampling companies, a group admin can create a subgroup for each company and assign the appropriate fields to each subgroup. 

Members are added to a subgroup in the same way as to a group. Additionally, if you are a member of a parent group you will automatically be a member of any subgroups that are created under that parent group. However, each subgroup can have its own members that are not members of the parent group.

### Create a subgroup
1. Navigate to the groups landing page
2. Select **ADD SUBGROUP**
3. Enter the group name and select **ADD GROUP**

![Add subgoupr](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Manage%20Groups/add_subgroup.jpg)

## Add Members to Groups and Subgroups
1. Select your group from the “Groups” menu. If adding a member to a subgroup, select the Subgroup from the Group Landing page.
2. Select **ADD MEMBER**
3. Enter the users email address and select **ADD MEMBER**

***Note**: To Remove members, select the trash icon next their email in the members list and then select **REMOVE** from the pop-up.*

![Add Member](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Manage%20Groups/add_member.jpg)
