# Try It Out!

## Create Demonstration Points
We have made it easy to test out SoilStack’s features. The following steps will walk through the process of creating demonstration points that can be used to try out SoilStack’s sample collection tools.

1. Create a field near you using the field drawing tools. For more information check out [this video](https://youtu.be/f-WCxeyyYd4) or our [tutorials](https://our-sci.gitlab.io/software/soilstack-tutorials/add_field/).
2. After you have drawn the field, go to that field’s information page. 
     - Click on `Generate Sampling Locations`
     - Select `Create Demo Locations`. This process will create 3 random sampling locations. These locations are **NOT** based on any stratification algorithm. 
3. Now you can use SoilStack’s in-field sampling tools to navigate to and collect samples at these locations. For more information on soil sampling check out [this video](https://youtu.be/XpNsx1UkJZY) or our [tutorials](https://our-sci.gitlab.io/software/soilstack-tutorials/soil_sampling/).

![Create demo](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/create_demo_points.png)

## Delete Demonstration Points
Now that you have created demonstration points, you may want to create an actual stratification for this field. To do this, you must first delete the demonstration points:

1. Scroll down to the **Stratifications** section of the *Field Information Page*.
2. Click on the `Trash` icon next to the demonstration points (you will see the word "demo" in the stratification name).
3. Select `Delete` from the pop-up. Note: deleting the demonstration points will also delete any sampling collection data associated with those points.

![delete demo](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/delete_demo_points.png)
