# Stratification Settings and Descriptions

[TOC]

## Field Buffers
**What is it?**

An area at the edge of the field that is masked prior to stratification so that 1) the area is not stratified and 2) sampling locations are not placed within this region. Our stratification approach uses vegetative indices from satellites along with elevation and digital soil maps to determine where to sample. Vegetative indices are very susceptible to edge of field effects like tree lines, grassy areas, etc. For this reason, a minimum buffer of 10 m is recommended to avoid artifacts of these edge-of-field effects in the stratification process (e.g., mixed pixels). Additionally, a larger buffer helps reduce the impact of these edge of field effects on carbon and ecosystem service modelling by avoiding area’s near tree lines, compacted headlands, etc.

**NOTE**: the buffer applies to all field boundaries, including cut-outs. Therefore, large buffers can mask significant portions of the field when fields are small, uniquely shaped, or have many cut-outs.

**Settings:**

- *Static* (type: "STATIC"): Enter a single value that applies to all field boundaries.
- *Dynamic* (type: "DYNAMIC"): A dynamic buffering algorithm is run to determine the appropriate buffer size for a given field boundary, of unique size and shape, by setting a minimum area of the field that should not be masked. This setting is best used when a larger buffer is preferred (e.g. 30 m) to avoid sampling near the edge of fields, but setting a static buffer size could mask out too large of an area in small fields or those with unique borders or lots of cut-outs.
- Dynamic settings:
     - *buffer_min* (m): The smallest buffer that can be used, we recommend 10 m.
     - *buffer_max* (m): The largest size buffer, we use 30 m for large fields.
     - *threshold* (ratio): The Minimum Sampleable area (% of field area) that should always be unmasked for sampling, unless the buffer needed to achieve that threshold is below the minimum buffer size. A threshold of 0.6 = 60% of the field area.
     - *buffer_interval* (m): The increment for changing the buffer size to meet the appropriate threshold. For example, using a 5 m increment (recommended), the buffering algorithm may determine that a buffer of at least 15 m is needed to achieve the minimum sampleable area. Conversely, an increment of 10 m would result in a buffer of 10 m being applied to the same field boundary.

**Status:** Available

## Clustering
**What is it?**

Clusters (or strata) are zones in the field that are similar based on the ancillary data layers used. You can choose to generate zones in your fields or to generate sample points by treating the whole field as a single unit. If you choose to generate clusters, a k-means clustering algorithm will generate like zones and output a raster map of the strata. If clustering is not used, then a cLHS algorithm is used to optimally place sampling locations across the field to sample for the variability observed in the ancillary data layers. More details about the clustering and cLHS processes are available here.

**Settings:**

- *Do not cluster*: The stratification service will treat the area as a single stratum and use cLHS to optimize placement of sample locations.
- *Set* (type: "STATIC"): Generate a specific number of clusters. Many MRV and/or sampling protocols call for a set number of strata. 
- *Dynamic* (type: "DYNAMIC"): Generates clusters based on the variability of the ancillary data layers. The range of potential clusters for a given field boundary are constrained by the area of the field, to prevent the service from generating too many small strata.  The settings that constrain this feature can be changed for each project using the dynamic settings below:
     - **Dynamic settings:**
          - *min*: minimum clusters that can be generated
          - *max*: maximum clusters that can be generated
          - *rate* (slope): The growth rate of clusters based on field size. For example, if the rate were 0.2, then a 40 hectare field would have a average of 8 clusters.
          - *min_area*
          - *max_area*
          - *span* (+/-): The number of potential clusters above and below the expected number of clusters. Using the example above, a 40 hectare field with a *rate* of 0.2 and a *span* of 8 could generate 4-12 clusters based on the variability of the ancillary data layers.
          - *offset* (y-axis)
          - *pad_min_length* (at least 4 points are needed for the service to determine the optimal number of clusters)

**Status:** The app currently supports all three types of clustering settings.

## Sample number
**What is it?**

The total number of sampling locations that should be generated for a field.

**Settings:**

- *Density* (1 point per X [acres or ha]): The number of sample points is determined based on the size of each stratum. Example:
     - *Density* = 1 sample per 3 acres.
     - The field has two strata
          - Stratum a = 7 acres
          - Stratum b = 15 acres
     - Total Sampling locations = 8, 3 in *a* and 5 in *b*
- *Set:* Select the number of sample points to place in each stratum (or field if “Do Not Cluster”). In this case, all strata have the same number of sample points, regardless of size.

**Status:** Available

**Notes:** Generation of sample points is based on stratum size, not field area, which can lead to differences between the expected number of sample points (based on field size) and the number of points output by the stratification service.

- The buffered area, which is masked prior to stratification, is not included when determining the number of sampling points. This can lead to fewer sampling points than expected based on the field size.
- The number of points per stratum is rounded up, so fields with many strata may end up with a few more points than expected based on the field size. 


## Sample Type
**What is it?**

How the soil samples are collected and aggregated in the field.

**Settings:**

- *Point Samples*: SoilStack will guide you to the sampling location. Once there, it captures the GPS coordinates of that central point. Depending on the sampling protocol, point sampling could include a single core at the designated sampling location, or it could include collecting multiple cores from the area around the sampling location. For example, some sampling protocols call for soil samplers to collect samples from the central point and from a point 10 ft away in four different directions.
- *Composite Samples*: All of the samples from a field or a stratum, at a given depth increment, are composited into a single soil sample for analysis at the laboratory.

**Status:** While the app best supports point sampling, composite sampling is possible (see notes below).

**Notes:** The front-end app displays the stratum ID, so a soil sampler can composite all samples from a given stratum into a single bag. However, they need to scan that sample bag ID at each sampling location in order to record the sampling locations.



