This page describes how [Our Sci LLC](https://www.our-sci.net/) handles Personal Data we receive or collect in relation to access and use of [SoilStack.io](https://www.soilstack.io/), [app.soilstack.io](https://app.soilstack.io/), and from our business contacts and other third parties who provide us personal data.

“Personal Data” as used in this Policy means information relating to an identified or identifiable natural person.

We may change this Policy from time to time in order to keep you apprised of our data collection and use practices. We encourage you to regularly visit and review this Policy to stay informed of such practices.

## SOILSTACK ROLES AND PERMISSIONS
SoilStack is a sampling and data collection tool with groups that determine who can see, modify, and delete field and sampling collection data. It's important to understand who has access to your data, and how it may be changed.

As a logged in user:

1. All members of a group will have equal rights to all field and sampling collection data within that group.
2. No user outside of your group will have access to any data from your group.

**As a USER you can choose to join or leave a group.** Make sure you trust the group and administrators with access to your fields and sampling collections.

## WHAT INFORMATION WE COLLECT AND HOW WE USE IT.
This section describes the ways in which we collect and receive information from outside our company and affiliates. It includes information on what Personal Data is collected, the purpose for which it is collected, to whom it is transferred, and the period for which it is retained. ‍

### Information you Provide
#### Creating or posting field data
‍When a field is created in SoilStack or posted to SoilStack via API, that field may include the following Personal Data:

1. GPS coordinates for the field boundary
2. Contact name
3. Contact address
4. Contact email
5. Contact phone number

#### Creating or posting sampling collection data
Stratifying a field, posting sampling locations to SoilStack via API, or creating sampling collections in SoilStack will generate:

1. GPS coordinates for target sampling locations
2. GPS coordinates for actual sampling coordinates

#### Platform Account
In order to use the Platform or the Site you will be required to create an account (“Account”). Your password is never shared.

#### Newsletter or Marketing Sign Up
If you sign up to receive our newsletters or other commercial or marketing communications from us on our Site, you may request that we stop sending you such communications at any time, including by using the “unsubscribe” link at the bottom of emails we send, at which point your email is removed from the list.

### Information we Automatically Collect
#### Cookies
We may use “cookies” to track activity on our Site. Cookies are small text files sent to your computer and then sent back by your Web browser to a website or other online service to retrieve the information in the cookie (for instance, whether you visited a website previously, or have already logged into an online account). “Session” cookies are temporary cookie files that last until you close your browser when they are deleted. “Persistent” cookies remain on your device until you or your browser delete them, or for the period set in the specific cookie. You may delete cookies or block cookies from your device, by changing the preferences in your browser. Doing so, however, may impair the functionality of the Services in whole or in part. ‍

#### Usage Data and Analytics Services
We may collect and analyze information about how you use and access our Site and the Platform such as when and how often users visit our Site and Platform and what pages or parts of the Site and Platform they visit and access.

## HOW (AND WITH WHOM) DO WE SHARE YOUR PERSONAL DATA
In addition to any methods or third parties with whom we may share your Personal Data described above, we share Personal Data in the following manner:

** Development.** We may also view your data when developing the software, inspecting or fixing the database, providing technical support, or other related internal development activities. These activities are limited to approved employees only and data is never shared with 3rd parties. This does not include passwords, but may include field and sampling collection information.

### YOUR GDPR RIGHTS
Subject to certain exceptions and exclusions, the following rights apply to individuals who are located in an EU member state or otherwise protected by the EU General Data Protection Regulation (“GDPR”), as further described below. If you are such a person, then:

Right of Access. You may request that we confirm to you whether or not we store Personal Data about you and to receive a copy of the Personal Data we maintain about you and information about: (a) the purposes of the processing of the Personal Data; (b) the categories of Personal Data being processed; (c) the names of the recipients or the categories of recipients to whom the Personal Data have been or will be disclosed, in particular recipients in third countries or international organizations; (d) if possible, the period we believe we will store the Personal Data, or the criteria we use to determine that period; (e) the sources of the Personal Data, if not collected from you; and (f) whether we use automated decision-making, including profiling, and meaningful information about the logic involved, as well as the significance and the envisaged consequences of such processing for you.

Right to Rectify. You may request that we correct any Personal Data about you that we maintain that is incorrect. Depending on the purpose for which the data is used, you may also request to complete incomplete Personal Data we maintain.

Right to Erasure (“Right to be Forgotten”). You may request that we erase or suppress Personal Data that relates to you in the following cases: the data is no longer needed by us; the data was collected and is used on the basis of your consent and you withdraw that consent; when you have a right to object to our use of the data (as described below under, “Right to Object”); we are not lawfully processing the data; or we are otherwise required by law to delete the data. However, there may be circumstances in which we may retain your data or we may refuse your request, for example, when we review the data to defend ourselves or make legal claims or exercise are own rights. In addition, this right may not apply to the display or access of your Personal Data outside of the European Union.

Right to Restrict Processing. You may request that we restrict our use or processing of your Personal Data if: you claim the Personal Data is inaccurate, during the time we investigate your claim; our processing of the Personal Data was unlawful; we no longer require the Personal Data; we processed the Personal Data for our legitimate interests and you object to this use (as you are permitted to do under Article 21(1) of the GDPR), during the time that we investigate whether our legitimate interests override your request. However, there may be circumstances in which we are legally entitled to refuse your request.

Right to Data Portability. You may request that we provide you with your Personal Data that we process based on your consent or to fulfill a contract with you or that we process using automated means, in a structured, commonly used and machine-readable format, and to transfer your Personal Data to another entity or have us transfer it directly to such entity.

Right to Object. You may, based on reasons specifically relating to you, object to our processing of your Personal Data, when: (i) the data is used for our legitimate interests and our interests in processing the data does not override your interests, rights and freedoms and we do not require use of the data for the establishment, exercise or defense of our legal claims or rights; and (ii) we use the data for direct marketing purposes or profiling for direct marketing purposes.

Right to Object to Automated Decision Making. You may request that you not to be subject to a decision based solely on automated processing, including profiling, when the decision produces legal effects concerning you or significantly affects you.

Right to Withdraw Consent. Where we process Personal Data relating to you based on your consent (such as by clicking a check box adjacent to a statement of consent), you may withdraw your consent and request that we cease using your Personal Data for the purpose for which you have your consent, or altogether, depending on the situation.

Right to Make a Complaint. You may file a complaint regarding our practices with the data protection authority in your place of habitual residence, place or work, or the place of the alleged infringement. For a list of data protection authorities in the European Economic Area, please see here: https://ec.europa.eu/newsroom/article29/item-detail.cfm?item_id=612080.

You can exercise your rights that apply to us by contacting us by email at info@soilstack.io.

We may be permitted by law (including the GDPR and local national laws) to refuse or limit the extent to which we comply with your request. We may also require additional information in order to comply with your request, including information needed to locate the information requested or to verify your identity or the legality of your request. To the extent permitted by applicable law, we may charge an appropriate fee to comply with your request. ‍

### YOUR CALIFORNIA PRIVACY RIGHTS‍
If you are a California resident you have the right under California law to make certain requests in connection with our use of Personal Data relating to you, as described below. To make such a request, please contact us. Please note that certain exceptions may apply.

Disclosure of Direct Marketing Practices. Under California Civil Code Section 1798.83, one time per year you may request the following information regarding our disclosure of your Personal Data to third parties for their direct marketing purposes: a list of the categories of the personal information disclosed to such parties during the preceding calendar year, the names and addresses of such third parties, and if the nature of the parties’ businesses is not clear from their names, examples of the products or services marketed by such third parties.

Removal of Public Information. If you are under the age of 18 and have an Account, under California Business and Professions Code Section 22581, you may request the removal of content or information you have publicly posted that is identified with you or your Account. Please be aware that certain exceptions may apply and we may not be able to completely remove all such information.

#### CALIFORNIA DO NOT TRACK NOTICE
We do not track individuals’ online activities over time and across third-party websites or online services (though we do receive information about the webpage you visited prior to accessing our websites, products and services such as our Site, the Platform, Support Site, and advertisement landing pages). We do not permit third-parties to track individuals’ online activities on our Site and Platform, unless it is part of a service provided to us (.e.g., Google Analytics). We do not respond to Web browser “do not track” signals or similar mechanisms.

### PERSONAL DATA OF CHILDREN
Our Services are not intended for, and we do not knowingly collect Personal Data from persons under the age of eighteen (18). If you believe that a person under the age of eighteen (18) has provided us with Personal Data, or if we have received the Personal Data of such person, please contact us at info@soilstack.io.

## DATA SECURITY
Data entered in SoilStack is stored on Amazon Web Services (AWS).. Group data is stored in a private database and only accessible by members of the group the data was submitted to.

## OUR CONTACT INFORMATION
For inquiries regarding this Policy, you may contact us as follows:

Our Sci LLC 600 S. Wagner Rd. Ann Arbor MI 48103 USA
