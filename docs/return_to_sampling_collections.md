# Returning to in progress Sampling Collections

## From the Field Information Page
### Using the "COLLECT" button
- Once you have entered your sampling depths and selected "START," you have created a Sampling Collection
- If you select "Collect" from the fields page and a Sampling Collection already exists, you can go directly to that “in progress” collection or start a new one.

![From fields](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Returning%20to%20Sample%20collection/from_field_page.png)

### Selecting from sampling collections
- You can also access the sampling collection from the "sampling collections" list at the bottom of the page

![Field Information Page](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Field_Information_Page.png)

## From the Sampling Collection Page

To return to or edit an existing Sampling Collection, select the sampling collection and then select EDIT.

![From sampling collection](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/Returning%20to%20Sample%20collection/from_sampling_collection_page.png)

## Transferring a Sampling Collection to a different device

You can EXPORT and Sampling Collection and IMPORT it onto a different device

On the device with the Sampling Collection:  
1. Navigate to Sampling Collections  
2. click on the field in progress  
3. click edit  
4. click on the number of samples in the upper left corner.  
5. click export  
6. Select "RAW JSON" and file type

![Transfer](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/transfer_collection.png)

![Transfer2](https://gitlab.com/our-sci/software/soilstack-tutorials/-/raw/master/images/transfer2.png)

