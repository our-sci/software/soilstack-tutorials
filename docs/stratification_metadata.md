## View/Download Stratification Metadata

From the Field Show page, scroll down to `stratifications` and select `View Details`. 

![View Stratification Metadata](https://gitlab.com/our-sci/resources/-/raw/master/images/SoilStack%20tutorials/View_Stratification_Metadata.jpg)

![Metadata Options](https://gitlab.com/our-sci/resources/-/raw/master/images/SoilStack%20tutorials/Metadata_Options.jpg)

From the pop-up modal, you can view or download:

1. View the number of locations and strata and the size of each stratum.
2. Download image files (below) for 
     - All of the anciallary data layers used in the stratification process
     - Strata boundaries
3. View detailed stratification metadata (bottom left).
4. View and Download a table of all sample locations (bottom right).

![Downloads](https://gitlab.com/our-sci/resources/-/raw/master/images/SoilStack%20tutorials/image_downloads.jpg)

![Table and Metadata](https://gitlab.com/our-sci/resources/-/raw/master/images/SoilStack%20tutorials/Points_and_metadata.jpg)

